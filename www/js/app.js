var app = new Vue({
  el: '#resthours',
  data: {
    daytime: "",
    message: "",
    error: false,
    results: []
  },
  methods: {
    search: async function (event) {
      this.results = [];
      this.error = false;
      this.message = "Loading...";
      event.preventDefault();
      try {
        res = await fetch( `/resthours?q=${this.daytime}` );
        if (res.ok) {
          this.error = false;
          this.results = await res.json();
          if ( !this.results.length ) {
            this.message = "No restaurants available " + this.daytime;
          } else {
            this.message = "";
          }
        } else {
          console.log( "Request failed with code", res.status, res );
          this.message = "ERROR: Invalid format. Please enter a 3-digit day then an hour or hour:minute then am or pm, separated by spaces.";
          this.error = true;
        }
      } catch( err ) {
        this.error = true;
        this.message = "Error fetching data";
        console.log( this.message, err );
      }
    },
    keyup: function (event) {
      const ENTER = 13;
      this.results = [];
      if ( event.keyCode == ENTER ) { this.search(event); }
    }
  }
});

