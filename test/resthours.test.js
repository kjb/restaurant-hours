const rh = require('resthours'),
      parse = require('rxparse');

const DAY_TIMES = [
  ['Mon 5 am', [0, 500]],
  ['Sun 12:30 am', [6, 30]],
  ['Fri 10:30 pm', [4, 2230]],
  ['Mon 12:00 am', [0, 0]],
  ['Mon 12:30 am', [0, 30]],
  ['Mon 1 am',     [0, 100]],
  ['Mon 11:30 am',  [0, 1130]],
  ['Mon 12 pm',  [0, 1200]],
  ['Mon 12:30 pm',  [0, 1230]],
  ['Mon 1:00 pm',  [0, 1300]],
  ['Mon 11:59 pm', [0, 2359]]
];

test.each(DAY_TIMES)('parse day time', (str, dayTime) => {
  expect( parse.getDayTime( str )).toStrictEqual( dayTime );
});

const restaurants = rh.getRestaurantSchedule(rh.readTimesFile());
function openNames(daytimestr) {
  let rest = restaurants.openRestaurants( daytimestr );
  return rest.map( it => it.name );
}

test('no open restaurants at 5am', () => {
  expect( openNames('Mon 5 am')).toStrictEqual([]);
});

test('open restaurants just before midnight', () => {
  expect( openNames('Thu 11:59 pm')).toContain( 'Bamboo Restaurant' );
});

test('closed restaurants at midnight', () => {
  expect( openNames('Thu 12 am')).not.toContain( 'Bamboo Restaurant' );
});

test('closed restaurants at midnight + 1', () => {
  expect( openNames('Thu 12:01 am')).not.toContain( 'Bamboo Restaurant' );
});

test('open after midnight friday night (saturday morning)', () => {
  let names = openNames('Sat 12:15 am');
  expect( names ).toContain( 'Sabella & La Torre' );
  expect( names ).not.toContain( 'Hanuri' );
});  

test('open noon', () => {
  let names = openNames( 'Sun 12 pm' );
  expect( names ).toEqual( expect.arrayContaining(["John's Grill", "Bamboo Restaurant"]));
});

