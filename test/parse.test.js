const rx = require('rxparse'),
      gr = require('grammarparse'),
      rh = require('resthours');

// array of the times from the JSON file. 
// note that each entry is an array of strings.
const TIMES = JSON.parse( rh.readTimesFile() ).map( ({name, times}) => times );

// parse with both parsers, and compare
function parse(s) {
  let r = rx.getDaysTimes(s),
      g = gr.getDaysTimes(s);
  expect( r ).toStrictEqual( g );
  return r;
}

test.each( TIMES )( 
  'parsers return same results', (...args) => {
    args.map( parse );
  });

test( 'days and single day ', () => {
  expect( parse( 'Mon-Thu, Sun 11:30 am - 9 pm' )).toStrictEqual( {days:[0,1,2,3,6], times: [1130,2100]} );
});

test( 'single day and days', () => {
  expect( parse( 'Mon, Wed-Sun 11 am - 10 pm' )).toStrictEqual( {days:[0,2,3,4,5,6], times: [1100,2200]} );
});
test( 'days and days', () => {
  expect( parse( 'Mon-Tue, Thu-Sun 11 am - 10 pm' )).toStrictEqual( {days:[0,1,3,4,5,6], times: [1100,2200]} );
});

test( 'noon and 12:30 pm', () => {
  expect( parse( 'Tue 12 pm - 12:30 pm' )).toStrictEqual( {days:[1], times: [1200,1230]} );
});

test( 'after midnight', () => {
  // don't span midnight, so low numbers
  expect( parse( 'Thu 12 am - 12:30 am' )).toStrictEqual( {days:[3], times: [0,30]} );
  expect( parse( 'Thu 12:30 am - 2 am' )).toStrictEqual( {days:[3], times: [30,200]} );
  expect( parse( 'Tue 12:30 am - 1:00 am' )).toStrictEqual( {days:[1], times: [30,100]} );
});

test( 'up to midnight', () => {
  // reaches midnight, so big number
  expect( parse( 'Tue 11:30 pm - 12:00 am' )).toStrictEqual( {days:[1], times: [2330,2400]} );
});

test( 'span midnight', () => {
  expect( parse( 'Sat 3 pm - 1:30 am' )).toStrictEqual( {days:[5], times:[1500,2530]} );
  expect( parse( 'Sat 11:59 pm - 12:30 am' )).toStrictEqual( {days:[5], times:[2359,2430]} );
  expect( parse( 'Tue 11:30 pm - 1:00 am' )).toStrictEqual( {days:[1], times: [2330,2500]} );
});

