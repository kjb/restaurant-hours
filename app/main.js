const express = require('express'),
      rh = require('resthours'),
      parse = require('rxparse');
      app = express(),
      port = process.env.PORT || 3000,
      interface = ('INTERFACE' in process.env ? process.env.INTERFACE : 'localhost');

const restaurants = rh.getRestaurantSchedule(rh.readTimesFile());

app.use('/', express.static('www'));
app.use('/js', express.static('src'));

app.get('/resthours', (req,res) => {
  const input = req.query.q;
  try {
    const data = restaurants.openRestaurants( input );
    res.send(data);
  } catch ( err ) {
    console.log( `Failed ${req.originalUrl} ${err}` );
    res.status( 400 ).send( {
      message: `ERROR: Invalid format. Please enter a 3-digit day then an hour or hour:minute then am or pm, separated by spaces.` 
    });
  }
});


app.listen( port, interface, () => {
  console.log( `Listening on http://${interface||'*'}:${port}` );
});
