# Restaurant Hours

resthours is a JavaScript implementation of the open restaurant hours project.

I included command-line and AJAX iterfaces, as well as tests for the
library, using code coverage. I also included a simple Vue-based UI to
interact with the node.js server, but didn't include tests that
exercise that UI or mock the fetch request.

To parse the day-time entries I implemented a simple parser using the
[Nearley](https://nearley.js.org/) algorithm, but I wasn't sure if the
[Nearley grammar](grammar/grammar.ne) would be friendly or
understandable to new dev team members, so I also included a
hand-coded, [regex-based parser](src/rxparse.js) as an alternate for comparison.

[Vue UI](https://jsfiddle.net/kbutler/t2dLves3/47/) in jsfiddle.

## Installation

Clone the repository, then npm install.

## Usage

```
Command-line client: 
  npm run cli 
  or
  node src/restcli [-v]
  Enter day-time strings as desired.
  Blank string dumps the restaurant list.
  -v (verbose) includes the restaurant open days and time ranges in the output.
  Ctrl-C or EOF to terminate

Run node server:
  npm run www
  request the root index file, e.g. [http://localhost:3000](http://localhost:3000)
```

## Sample interactive output
```text
$ node src/restcli
Enter day and time (e.g., Mon 10:30 am): Mon 5 am
Restaurants open at Mon 5 am:



Enter day and time (e.g., Mon 10:30 am): Mon 10:30 am
Restaurants open at Mon 10:30 am:
        Canton Seafood & Dim Sum Restaurant
        All Season Restaurant
        Herbivore
        Sabella & La Torre
        Tong Palace
        India Garden Restaurant
        Santorini's Mediterranean Cuisine


Enter day and time (e.g., Mon 10:30 am): Mon 12 am
Restaurants open at Mon 12 am:
        Naan 'N' Curry
        Thai Stick Restaurant
        Marrakech Moroccan Restaurant


Enter day and time (e.g., Mon 10:30 am):

Closed. Do the dishes and go home.
```
## Tests
```python
npm test          # runs automated tests, including coverage
npm run testcli   # runs some pseudo-interactive tests
npm run testwww   # simple curl test to exercise node server

```

## Other scripts
```
JSHint linting:
  npm run hint

Re-generate the grammar.js file from grammar.ne:
  npm run nearleyc
```
## License
[MIT](https://choosealicense.com/licenses/mit/)
