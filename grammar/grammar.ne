# The structure of the day-time strings is:
#
#   OPEN_DAYS_HOURS: DAYS TIME_RANGE
#   DAYS: DAY_RANGE[, DAY_RANGE]
#   DAY_RANGE: DAY[-DAY]
#   DAY: (Mon|Tue|Wed|Thu|Fri|Sat|Sun)
#   TIME_RANGE: TIME - TIME   # open and close are mandatory
#   TIME: HOUR[:MIN] [am|pm]
#   HOUR: /d[/d]
#   MIN: /d/d  # actually only "30"

# I encoded this using the nearley.js parser generator. Using a parser
# for this is somewhat overkill, and I'm not sure if this will be more
# or less clear to members of the team than a hand-coded parser.  It
# is an additional concept/library/DSL to learn, but is handy for harder problems.

# 
# Basic structure of the grammar rules is:
#
#   PATTERN -> PATTERN1 PATTERN2 ...   {% postprocessFunction %}
# 
# As each pattern rule is applied, the parser generates the array of completed patterns
# that matched the data, so it recursively generates a tree of nested arrays.  
#
# Each rule can optionally specify a postprocess function to transform
# the results array into a more convenient value. 
#
#     {% ([pattern1data, pattern2data, ...]) => transformedValues %}
#
# Specific notes:
# 
# - {% id %} unwraps a single element from the resultsArray, equivalent to  {% d => d[0] %}
# 
# - groups of patterns within a rule create an additional level of nesting:
#       (a | b), resultsArray will be [[whatever]], so d[0][0] === whatever
#   this is because the rule could have more than just the group
#
# - we define a rule to map '_' to optional whitespace. The actual JSON data uses consistent whitespace.
#
# Can play with this parser interactively by copy and paste to 
# https://omrelli.ug/nearley-playground/




# Parse days and time range string
#
# Postprocess function in {%...%} converts array to a map:
#  days: [ [startday, endday], ... ] # may have multiple ranges of days, and startday may == endday
#  times: [ start, end ]
#
# note that end time may be morning of the next day, so restaurant may
# actually be open on next day, too.
OPEN_DAYS_HOURS  -> DAYS _ TIME_RANGE    {% d => ({ days:d[0], times:d[2]}) %}

# days that restaurant opens
# returned as array of day numbers, with Monday=0, ...
DAYS       -> DAY_RANGE "," _ DAY_RANGE  {% d => d[0].concat( d[3] ) %}
            | DAY_RANGE                  {% id %}

# day range is a list of [start to  end, inclusive]
# note the single day case is returned as [startday, startday] range, with start and end the same day
DAY_RANGE  -> DAY "-" DAY                {% d => numericDays( d[0], d[2] ) %}
            | DAY                        {% d => numericDays( d[0], d[0] ) %}

DAY        -> ( "Mon" | "Tue" | "Wed" | "Thu" | "Fri" | "Sat" | "Sun" )            
                                         {% d => d[0][0] /* extra nesting */ %}

# end times after midnight should be 2400+
TIME_RANGE -> TIME _ "-" _ TIME          {% d => [ d[0], d[4] < d[0] ? d[4] + MIDNIGHT_MINUTES : d[4] ] %}
TIME       -> HOUR ":" MIN _ AM_PM       {% ([hour, _colon, min, _, ampm]) => calcTime(hour, min, ampm) %}
            | HOUR _ AM_PM               {% ([hour, _, ampm]) => calcTime(hour, 0, ampm) %}

HOUR       -> NUM            {% id %}
MIN        -> NUM            {% id %}
AM_PM      -> ("am" | "pm")  {% d => d[0][0] /* extra nesting */ %}

# Numbers. We only need 1 or 2 digits, and only positive numbers.
NUM -> [0-9]:+               {% d => parseInt(d[0].join("")) %}

# _ represents optional whitespace, ignored by returning null
_          -> [\s]:*         {% _ => null %}


@{%
////////// Utility functions

const NOON_MINUTES = 1200,
      MIDNIGHT_MINUTES = 2400,
      NOON_MIDNIGHT = 12;

function calcTime(hour, min, ampm) {
  let h = parseInt(hour);
  h = h === NOON_MIDNIGHT ? 0 : h; // 12pm == 0pm, 12am = 0am
  return h * 100 + parseInt(min) + (ampm === "pm" ? NOON_MINUTES : 0);
}

function numericDays(startDay, endDay) {
  let days = [];
  for (let i = dayValue(startDay); i <= dayValue(endDay); i++) {
    days.push(i);
  }
  return days;
}

function dayValue(d) {
  const DAYS = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
  return DAYS.indexOf(d);
}
%}