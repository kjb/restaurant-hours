const readline = require('readline'),
      rh = require('./resthours.js'),
      parse = require('./rxparse.js');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  prompt: 'Enter day and time (e.g., Mon 10:30 am): '
});

const restaurants = rh.getRestaurantSchedule(rh.readTimesFile());
const verbose = process.argv.includes( '-v' );
rh.verbose = verbose;

rl.prompt();
rl.on('line', (input) => {
  try {
    verbose && input && console.log( `checking ${input} ${parse.getDayTime( input )}` );
    let rest = input ? restaurants.openRestaurants( input ) : restaurants.restaurantTimes;
    let list = verbose ? rest : rest.map( it => it.name );
    console.log( `Restaurants open ${input}:\n\t${list.join("\n\t")}\n\n`);

  } catch ( err ) {
    console.log( "Invalid format. Please enter a 3-digit day then an hour or hour:minute then am or pm, separated by spaces." );
  }

  rl.prompt();
});

rl.on('close', () => {
  console.log( '\n\nClosed. Do the dishes and go home.' );
});


