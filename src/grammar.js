// Generated automatically by nearley, version 2.19.1
// http://github.com/Hardmath123/nearley
(function () {
function id(x) { return x[0]; }

////////// Utility functions

const NOON_MINUTES = 1200,
      MIDNIGHT_MINUTES = 2400,
      NOON_MIDNIGHT = 12;

function calcTime(hour, min, ampm) {
  let h = parseInt(hour);
  h = h === NOON_MIDNIGHT ? 0 : h; // 12pm == 0pm, 12am = 0am
  return h * 100 + parseInt(min) + (ampm === "pm" ? NOON_MINUTES : 0);
}

function numericDays(startDay, endDay) {
  let days = [];
  for (let i = dayValue(startDay); i <= dayValue(endDay); i++) {
    days.push(i);
  }
  return days;
}

function dayValue(d) {
  const DAYS = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
  return DAYS.indexOf(d);
}
var grammar = {
    Lexer: undefined,
    ParserRules: [
    {"name": "OPEN_DAYS_HOURS", "symbols": ["DAYS", "_", "TIME_RANGE"], "postprocess": d => ({ days:d[0], times:d[2]})},
    {"name": "DAYS", "symbols": ["DAY_RANGE", {"literal":","}, "_", "DAY_RANGE"], "postprocess": d => d[0].concat( d[3] )},
    {"name": "DAYS", "symbols": ["DAY_RANGE"], "postprocess": id},
    {"name": "DAY_RANGE", "symbols": ["DAY", {"literal":"-"}, "DAY"], "postprocess": d => numericDays( d[0], d[2] )},
    {"name": "DAY_RANGE", "symbols": ["DAY"], "postprocess": d => numericDays( d[0], d[0] )},
    {"name": "DAY$subexpression$1$string$1", "symbols": [{"literal":"M"}, {"literal":"o"}, {"literal":"n"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "DAY$subexpression$1", "symbols": ["DAY$subexpression$1$string$1"]},
    {"name": "DAY$subexpression$1$string$2", "symbols": [{"literal":"T"}, {"literal":"u"}, {"literal":"e"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "DAY$subexpression$1", "symbols": ["DAY$subexpression$1$string$2"]},
    {"name": "DAY$subexpression$1$string$3", "symbols": [{"literal":"W"}, {"literal":"e"}, {"literal":"d"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "DAY$subexpression$1", "symbols": ["DAY$subexpression$1$string$3"]},
    {"name": "DAY$subexpression$1$string$4", "symbols": [{"literal":"T"}, {"literal":"h"}, {"literal":"u"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "DAY$subexpression$1", "symbols": ["DAY$subexpression$1$string$4"]},
    {"name": "DAY$subexpression$1$string$5", "symbols": [{"literal":"F"}, {"literal":"r"}, {"literal":"i"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "DAY$subexpression$1", "symbols": ["DAY$subexpression$1$string$5"]},
    {"name": "DAY$subexpression$1$string$6", "symbols": [{"literal":"S"}, {"literal":"a"}, {"literal":"t"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "DAY$subexpression$1", "symbols": ["DAY$subexpression$1$string$6"]},
    {"name": "DAY$subexpression$1$string$7", "symbols": [{"literal":"S"}, {"literal":"u"}, {"literal":"n"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "DAY$subexpression$1", "symbols": ["DAY$subexpression$1$string$7"]},
    {"name": "DAY", "symbols": ["DAY$subexpression$1"], "postprocess": d => d[0][0] /* extra nesting */},
    {"name": "TIME_RANGE", "symbols": ["TIME", "_", {"literal":"-"}, "_", "TIME"], "postprocess": d => [ d[0], d[4] < d[0] ? d[4] + MIDNIGHT_MINUTES : d[4] ]},
    {"name": "TIME", "symbols": ["HOUR", {"literal":":"}, "MIN", "_", "AM_PM"], "postprocess": ([hour, _colon, min, _, ampm]) => calcTime(hour, min, ampm)},
    {"name": "TIME", "symbols": ["HOUR", "_", "AM_PM"], "postprocess": ([hour, _, ampm]) => calcTime(hour, 0, ampm)},
    {"name": "HOUR", "symbols": ["NUM"], "postprocess": id},
    {"name": "MIN", "symbols": ["NUM"], "postprocess": id},
    {"name": "AM_PM$subexpression$1$string$1", "symbols": [{"literal":"a"}, {"literal":"m"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "AM_PM$subexpression$1", "symbols": ["AM_PM$subexpression$1$string$1"]},
    {"name": "AM_PM$subexpression$1$string$2", "symbols": [{"literal":"p"}, {"literal":"m"}], "postprocess": function joiner(d) {return d.join('');}},
    {"name": "AM_PM$subexpression$1", "symbols": ["AM_PM$subexpression$1$string$2"]},
    {"name": "AM_PM", "symbols": ["AM_PM$subexpression$1"], "postprocess": d => d[0][0] /* extra nesting */},
    {"name": "NUM$ebnf$1", "symbols": [/[0-9]/]},
    {"name": "NUM$ebnf$1", "symbols": ["NUM$ebnf$1", /[0-9]/], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "NUM", "symbols": ["NUM$ebnf$1"], "postprocess": d => parseInt(d[0].join(""))},
    {"name": "_$ebnf$1", "symbols": []},
    {"name": "_$ebnf$1", "symbols": ["_$ebnf$1", /[\s]/], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "_", "symbols": ["_$ebnf$1"], "postprocess": _ => null}
]
  , ParserStart: "OPEN_DAYS_HOURS"
}
if (typeof module !== 'undefined'&& typeof module.exports !== 'undefined') {
   module.exports = grammar;
} else {
   window.grammar = grammar;
}
})();
