// Restaurant hours module
// Usage:
//   readTimesFile().openRestaurants( 'Mon', '10:30 am' )
//   returns list of restauarants open at that time
const fs = require('fs'),
      assert = require('assert'),
      parse = require('./rxparse');

let verbose = false;

function timeMatch( entry, day, time ) {
  // restaurant is open if open on the day and at that time. 
  // After midnight closes will be 2400+, so check if we are open after midnight from the previous day
  let match = (entry.days.includes(day) && time >= entry.times[0] && time < entry.times[1]) ||
      (entry.days.includes( previousDay( day )) && (time + parse.MIDNIGHT_MINUTES) < entry.times[1]);

  verbose && console.log( 'timeMatch', match, 'testing:', day, time, 'vs', entry );
  return match;
}

function previousDay( day ) {
  return (day + 6) % 7;
}

class Restaurant {
  constructor( name, daystimeslist ) {
    this.name = name;
    this.daystimes = daystimeslist.map( s => parse.getDaysTimes( s )); // list of [{days,times}]
  }

  isOpen( day, time ) {
    verbose && console.log( `isOpen ${day} ${time} ${this}` );
    let open = this.daystimes.find( entry => timeMatch( entry, day, time ));
    return open;
  }

  toString() {
    return this.name + ': ' + this.daystimes.map( ({days,times}) => `[${days}:${times[0]}-${times[1]}]` );
  }
}

class RestaurantSchedule {
  constructor( restaurantTimes ) {
    this.restaurantTimes = restaurantTimes;
  }

  openRestaurants( daytimestr ) {
    let [day, time] = parse.getDayTime( daytimestr );
    return this.restaurantTimes.filter( restaurant => restaurant.isOpen( day, time ));
  }

  
}

function readTimesFile( path='data/rest_hours.json' ) {
  return fs.readFileSync( path, 'utf8' );
}

function getRestaurantSchedule( str ) {
  return new RestaurantSchedule( parseRestaurants( JSON.parse( str )));
}

function parseRestaurants( timesData ) {
  return timesData.map( ({name, times}) => new Restaurant( name, times )); 
}

const rh = {
  readTimesFile: readTimesFile,
  getRestaurantSchedule: getRestaurantSchedule,
  verbose: verbose
};

if (typeof module !== 'undefined'&& typeof module.exports !== 'undefined') {
  module.exports = rh;
} else {
  window.rh = rh;
}
