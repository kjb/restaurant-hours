const nearley = require('nearley'),
      grammar = require('./grammar');

function getDaysTimes(s) {
    const parser = new nearley.Parser(nearley.Grammar.fromCompiled(grammar));
    parser.feed( s );
    if ( !(parser.results && parser.results[0]) ) {
        throw new Error( "Unexpected end of input", parser );
    }
    return parser.results[0]; // ambiguous grammars return multiple results. we only care about first
    // ignoring parse errors
}

const parse = {
    getDaysTimes: getDaysTimes
};

if (typeof module !== 'undefined'&& typeof module.exports !== 'undefined') {
  module.exports = parse;
} else {
  window.parse = parse;
}
