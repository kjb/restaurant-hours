/*
 * Parse days and times, returning object:
 *   days: array of day numbers, with Monday=0, etc.
 *   times: array of startTime, endTime in pseudo military time
 */
function getDaysTimes(s) {
  let [days, rest] = getDays(s),
      times = getTimes(rest);
  return { days: days, times: times };
}

/*
 * getDayTime from user input string, or null for no match
 * 
 * not completely robust input validation...
 */
function getDayTime( input ) {
  let day = getDays(input)[0][0],
      time = getTime(input)[0];
  if ((day < 0 || day > 6 || time < 0 || time > 2400)) {
    throw new RangeError( "Invalid day time" );
  }
  return [day, time];
}

/*
 * Parse day lists out of string. May skip preceding characters, like commas and spaces.
 * Returns: [ list of numeric days, with Monday 0, etc, rest of string ]
 */
function getDays(s) {
  let 
  [startDay, endDay, rest] = getDayRange(s),
  days = numericDays(startDay, endDay);
  if (rest[0] === ",") { // parse and add remaining days
    let restDays;
    [restDays, rest] = getDays(rest);
    days.push(...restDays);
  } 
  return [days, rest];
}

/*
 * Return start and end times as 24-hour time.
 * end times after midnight should be 2400+
 */
function getTimes(s) {
  let [start, rest] = getTime(s),
      [end, _] = getTime(rest);
  return [start, end < start ? end + MIDNIGHT_MINUTES : end];
}

/*
 * Parse a time entry as 24-hour time, possibly skipping characters before numbers.
 */
function getTime(s) {
  // Regular expression to capture time:
  // skip whitespace and hyphen
  // 1 capture hours digits
  // skip :, if present
  // 2 capture minutes digits
  // 3 capture am/pm
  const TIME_RX = /(\d+):?(\d+)? (am|pm)/;
  let match = s.match( TIME_RX ),
      [hour, min, ampm] = [match[1], match[2] || "0", match[3]],
      time = calcTime( hour, min, ampm ),
      rest = getRest( s, match );
  return [time, rest];
}

/*
 * Parse a day range.
 *
 * return [startDay, endDay, restOfString]
 *
 * Note that single days will come back as if written "Mon-Mon".
 */
function getDayRange(s) {
  let [startDay, rest] = getDay(s),
      endDay = startDay;
  if (rest[0] == "-") { // then it is a range
    [endDay, rest] = getDay(rest);
  }
  return [startDay, endDay, rest];
}

function getDay(s) {
  const DAYS_RX = /(Mon|Tue|Wed|Thu|Fri|Sat|Sun)/;
  let match = s.match(DAYS_RX);
  if (match) {
    return [match[1], getRest( s, match )];
  } else {
    return [null, s];
  }
}

/*
 * Return the rest of a string, following a match.
 */
function getRest( s, match ) {
  return s.slice( match.index + match[0].length );
}

////////// Utility functions

const NOON_MINUTES = 1200,
      MIDNIGHT_MINUTES = 2400,
      NOON_MIDNIGHT = 12;

function calcTime( hour, min, ampm ) {
  let h = parseInt(hour);
  h = h == NOON_MIDNIGHT ? 0 : h; // 12pm == 0pm, 12am = 0am
  return h * 100 + parseInt(min) + (ampm == "pm" ? NOON_MINUTES : 0);
}

function numericDays(startDay, endDay) {
  let days = [];
  for (let i = dayValue(startDay); i <= dayValue(endDay); i++) {
    days.push(i);
  }
  return days;
}

function dayValue(d) {
  const DAYS = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
  return DAYS.indexOf(d);
}

const parse = {
  getDaysTimes: getDaysTimes,
  getDayTime: getDayTime,
  MIDNIGHT_MINUTES: MIDNIGHT_MINUTES
};

if (typeof module !== 'undefined'&& typeof module.exports !== 'undefined') {
  module.exports = parse;
} else {
  window.parse = parse;
}
